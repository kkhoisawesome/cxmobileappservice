﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using CXDayMobileAppService.DataObjects;
using CXDayMobileAppService.Models;
using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.Mobile.Server.Config;

namespace CXDayMobileAppService.Controllers
{
    public class CardsController : TableController<Cards>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            CXDayMobileAppContext context = new CXDayMobileAppContext();
            DomainManager = new EntityDomainManager<Cards>(context, Request);
        }

        public IQueryable<Cards> GetAllCards()
        {
            return Query();
        }

        public SingleResult<Cards> GetCardItem(string id)
        {
            return Lookup(id);
        }

        public Task<Cards> PatchCard(string id, Delta<Cards> patch)
        {
            return UpdateAsync(id, patch);
        }

        public async Task<IHttpActionResult> PostCardItem(Cards item)
        {
            Cards current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        public Task DeleteCardItem(string id)
        {
            return DeleteAsync(id);
        }
    }
}