﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Azure.Mobile.Server;

namespace CXDayMobileAppService.DataObjects
{
    public class Cards : EntityData
    {
        public string Title { get; set; }
        public string Authors { get; set; }
        public string Image { get; set; }
        public string DescriptionInHtml { get; set; }
    }
}